"""
==============================
BAG2 resistor parameter module
==============================

The resistor parameter template module of BAG2 framework.

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 12.4.2022.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""
import os
import pkg_resources
from typing import *
import yaml

# To use this file as starting point for layers_parameters class comment the following lines
from abc import ABCMeta, abstractmethod


class resistor_parameters_template(metaclass=ABCMeta):

    # Then  uncomment the followint lines
    # from BAG2_technology_template.resistor_parameters_template import resistor_parameters_template
    # class resistor_parameters(resistor_parameters_template):

    # Then, delete the lines with '@abstractmethod' and start filling in the right values

    @property
    @abstractmethod
    def bot_layer(self) -> int:
        """Bottom horizontal routing layer ID

        """
        return int

    @property
    @abstractmethod
    def block_pitch(self) -> Tuple[int, int]:
        """Resistor core block pitch in resolution units

        """
        return int, int

    @property
    @abstractmethod
    def po_sp(self) -> int:
        """Space between PO and dummy PO

        """
        return int

    @property
    @abstractmethod
    def imp_od_sp(self) -> int:
        """Space between implant layer and OD.  
        Used only if OD cannot be inside resistor implant.

        """
        return int

    @property
    @abstractmethod
    def po_od_sp(self) -> int:
        """Space between PO/dummy PO and dummy OD

        """
        return int

    @property
    @abstractmethod
    def po_co_enc(self) -> Tuple[int, int]:
        """PO horizontal/vertical enclosure of CONTACT

        """
        return int, int

    @property
    @abstractmethod
    def po_rpo_ext_exact(self) -> int:
        """Exact extension of PO over RPO.  If negative, this parameter is ignored.

        """
        return -int

    @property
    @abstractmethod
    def po_max_density(self) -> float:
        """Maximum PO density (recommended)

        """
        return float

    @property
    @abstractmethod
    def dpo_dim_min(self) -> Tuple[int, int]:
        """Dummy PO minimum width/height

        """
        return int, int

    @property
    @abstractmethod
    def od_dim_min(self) -> Tuple[int, int]:
        """Dummy OD minimum width/height

        """
        return int, int

    @property
    @abstractmethod
    def od_dim_max(self) -> Tuple[int, int]:
        """Dummy OD maximum width/height

        """
        return int, int

    @property
    @abstractmethod
    def od_sp(self) -> int:
        """Dummy OD space

        """
        return int

    @property
    @abstractmethod
    def od_min_density(self) -> float:
        """Minimum OD density

        """
        return float

    @property
    @abstractmethod
    def co_w(self) -> int:
        """CONTACT width

        """
        return int

    @property
    @abstractmethod
    def co_sp(self) -> int:
        """CONTACT spacing

        """
        return int

    @property
    @abstractmethod
    def m1_co_enc(self) -> Tuple[int, int]:
        """METAL1 horizontal/vertical enclosure of CONTACT

        """
        return int, int

    @property
    @abstractmethod
    def m1_sp_max(self) -> int:
        """METAL1 fill maximum spacing

        """
        return int

    @property
    @abstractmethod
    def m1_sp_bnd(self) -> int:
        """METAL1 fill space to boundary

        """
        return int

    @property
    @abstractmethod
    def rpo_co_sp(self) -> int:
        """Space of RPO to CONTACT

        """
        return int

    @property
    @abstractmethod
    def rpo_extx(self) -> int:
        """Extension of RPO on PO

        """
        return int

    @property
    @abstractmethod
    def edge_margin(self) -> int:
        """Margin needed on the edges

        """
        return int

    @property
    @abstractmethod
    def imp_enc(self) -> Tuple[int, int]:
        """Enclosure of implant layers in horizontal/vertical direction

        """
        return int, int

    @property
    @abstractmethod
    def imp_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        """Resistor implant layers list

        """
        return {
            'nch': {
                ('layer', 'purpose'): [int, int],
                ('layer', 'purpose'): [int, int],
            },
            'pch': {
                ('layer', 'purpose'): [int, int],
                ('layer', 'purpose'): [int, int],
                ('layer', 'purpose'): [int, int],
            },
            'ptap': {
                ('layer', 'purpose'): [int, int],
                ('layer', 'purpose'): [int, int],
            },
            'ntap': {
                ('layer', 'purpose'): [int, int],
                ('layer', 'purpose'): [int, int],
                ('layer', 'purpose'): [int, int],
            }
        }

    @property
    @abstractmethod
    def res_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        return {
            'standard': {
                ('layer', 'purpose'): [int, int],
            },
            'high_speed': {
                ('layer', 'purpose'): [int, int],
                ('layer', 'purpose'): [int, int],
            }
        }

    @property
    @abstractmethod
    def thres_layers(self) -> Dict[str, Dict[str, Dict[Tuple[str, str], List[int]]]]:
        return {
            'ptap': {
                'standard': {},
                'svt': {},
                'lvt': {
                    ('layer', 'purpose'): [0, 0],
                },
                'ulvt': {
                    ('layer', 'purpose'): [0, 0],
                },
                'fast': {
                    ('layer', 'purpose'): [0, 0],
                },
                'hvt': {
                    ('layer', 'purpose'): [0, 0],
                },
                'uhvt': {
                    ('layer', 'purpose'): [0, 0],
                }
            },
            'ntap': {
                'standard': {},
                'svt': {},
                'lvt': {
                    ('layer', 'purpose'): [0, 0],
                },
                'ulvt': {
                    ('layer', 'purpose'): [0, 0],
                },
                'fast': {
                    ('layer', 'purpose'): [0, 0],
                },
                'hvt': {
                    ('layer', 'purpose'): [0, 0],
                },
                'uhvt': {
                    ('layer', 'purpose'): [0, 0],
                },
            }
        }

    @property
    @abstractmethod
    def info(self) -> Dict:
        """Resistor type information dictionary
  
        """
        return {
            'standard': {
                'rsq': float,
                'min_nsq': int,
                'w_bounds': (float, float),
                'l_bounds': (float, float),
                # True to draw RPO layer, which is a layer that makes
                # PO resistive.
                # True to draw RPDMY layer, which is a layer directly
                'need_rpo': Bool,
                # on top of the resistor.  Usually for LVS purposes.
                'need_rpdmy': Bool,
                # True if OD can be drawn in resistor implant layer.
                'od_in_res': Bool,
            },
            'high_speed': {
                'rsq': float,
                'min_nsq': float,
                'w_bounds': (float, float),
                'l_bounds': (float, float),
                # True to draw RPO layer, which is a layer that makes
                # PO resistive.
                # True to draw RPDMY layer, which is a layer directly
                'need_rpo': Bool,
                # on top of the resistor.  Usually for LVS purposes.
                'need_rpdmy': Bool,
                # True if OD can be drawn in resistor implant layer.
                'od_in_res': Bool,
            }
        }

    @property
    @abstractmethod
    def property_dict(self) -> Dict:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        devel = False

        if not hasattr(self, '_property_dict'):
            if devel:
                yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
                with open(yaml_file, 'r') as content:
                    # self._process_config = {}
                    dictionary = yaml.load(content, Loader=yaml.FullLoader)

                self._property_dict = dictionary['resistor']
            else:
                self._property_dict = {}

        for key, val in vars(type(self)).items():
            if isinstance(val, property) and key != 'property_dict':
                try:
                    self._property_dict[key] = getattr(self, key)
                except NotImplementedError:
                    pass  # skip unimplemented parameters (i.e. FINFET for a CMOS tech)
        return self._property_dict
